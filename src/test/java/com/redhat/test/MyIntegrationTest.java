package com.redhat.test;

import io.quarkus.test.junit.QuarkusTest;
import io.reactivex.subscribers.TestSubscriber;
import org.eclipse.microprofile.reactive.messaging.Channel;
import org.junit.jupiter.api.Test;
import org.reactivestreams.Publisher;

import javax.inject.Inject;

@QuarkusTest
public class MyIntegrationTest {
    @Inject
    @Channel("data")
    Publisher<String> emailReceiver;

    @Inject
    MainProcess mainProcess;

    @Test
    public void testSecond() {
        TestSubscriber<String> subscriber = new TestSubscriber<>();
        emailReceiver.subscribe(subscriber);
        mainProcess.imaginaryProcess();

        subscriber.awaitCount(1);
        subscriber.assertValueCount(1);
    }
}
