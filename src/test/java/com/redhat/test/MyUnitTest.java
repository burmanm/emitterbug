package com.redhat.test;

import com.redhat.test.sub.MyKafkaListener;
import io.quarkus.test.junit.QuarkusTest;
import io.reactivex.subscribers.TestSubscriber;
import org.eclipse.microprofile.reactive.messaging.Channel;
import org.junit.jupiter.api.Test;
import org.reactivestreams.Publisher;

import javax.inject.Inject;

@QuarkusTest
public class MyUnitTest {
    @Inject
    @Channel("data")
    Publisher<String> emailReceiver;

    @Inject
    MyKafkaListener kafkaListener;

    @Test
    public void testFirst() {
        TestSubscriber<String> subscriber = new TestSubscriber<>();
        emailReceiver.subscribe(subscriber);
        kafkaListener.process();

        subscriber.awaitCount(1);
        subscriber.assertValueCount(1);
    }

}
