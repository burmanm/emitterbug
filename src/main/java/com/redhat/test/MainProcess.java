package com.redhat.test;

import com.redhat.test.sub.MyKafkaListener;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

@ApplicationScoped
public class MainProcess {

    @Inject
    MyKafkaListener kafkaListener;

    public void imaginaryProcess() {
        kafkaListener.process();
    }
}
