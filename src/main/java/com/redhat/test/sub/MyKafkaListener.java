package com.redhat.test.sub;

import io.smallrye.reactive.messaging.annotations.Channel;
import io.smallrye.reactive.messaging.annotations.Emitter;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

@ApplicationScoped
public class MyKafkaListener {

    @Inject
    @Channel("data")
    Emitter<String> dataChan;

    public void process() {
        String test = "testString";
        dataChan.send(test);
    }
}
